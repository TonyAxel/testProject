<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ViewController;
use App\Http\Controllers\CreateController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/material/{id}', [ViewController::class, 'viewMaterial'])->name('viewMaterial');
Route::get('/create_tag',[ViewController::class, 'viewCreateTag'])->name('createTag');
Route::get('/list_tag',[ViewController::class, 'viewListTag'])->name('listTag');
Route::get('/create_material',[ViewController::class, 'viewCreateMaterial'])->name('createMaterial');
Route::get('/',[ViewController::class, 'viewListMaterial'])->name('listMaterial');
Route::get('/create_category',[ViewController::class, 'viewCreateCategory'])->name('createCategory');
Route::get('/list_category',[ViewController::class, 'viewListCategory'])->name('listCategory');
Route::get('/update/{id}', [ViewController::class, 'viewUpdateMaterial'])->name('UpdateMaterial');
Route::get('/delete/{id}',[CreateController::class, 'deleteMaterial'])->name('deleteMaterial');
Route::get('/delete_tag_material/{idTag}/{id}', [CreateController::class, 'deleteTaginMaterial'])->name('deleteTaginMaterial');
Route::get('delete_href_material/{idHref}/{id}', [CreateController::class, 'deleteHrefinMaterial'])->name('deleteHrefinMaterial');
Route::get('/delete_tag/{id}', [CreateController::class, 'deleteTag'])->name('deleteTag');
Route::get('/view_update_tag/{id}', [ViewController::class, 'viewUpdateTag'])->name('viewUpdateTag');
Route::get('/view_update_category/{id}', [ViewController::class, 'viewUpdateCategory'])->name('viewUpdateCategory');
Route::get('/delete_category/{id}', [CreateController::class, 'deleteCategory'])->name('deleteCategory');
Route::get('/view_search_tag/{tag}', [ViewController::class, 'viewSearchTag'])->name('viewSearchTag');
Route::post('/add_material', [CreateController::class, 'createMaterial'])->name('addMaterial');
Route::post('/up_material/{id}', [CreateController::class, 'updateMaterial'])->name('update');
Route::post('/search', [ViewController::class, 'search'])->name('search');
Route::post('/add_tag_material/{id}', [CreateController::class, 'addTaginMaterial'])->name('addTaginMaterial');
Route::post('/add_href/{id}', [CreateController::class, 'addHref'])->name('addHref');
Route::post('/add_category', [CreateController::class, 'addCategory'])->name('addCategory');
Route::post('/add_tag', [CreateController::class, 'addTag'])->name('addTag');
Route::post('/update_tag/{id}', [CreateController::class, 'updateTag'])->name('updateTag');
Route::post('/update_category/{id}', [CreateController::class, 'updateCategory'])->name('updateCategory');


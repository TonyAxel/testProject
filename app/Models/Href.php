<?php
/**
 * @package App\Models
 * 
 * @property string $name
 * @property string $href
 * @property int $material_id Foreign key таблицы Material
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Href extends Model
{
    use HasFactory;
    protected $table = 'hrefs';
    protected $fillable = ['name', 'href', 'material_id'];
}

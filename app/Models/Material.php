<?php
/**
 * @package App\Models
 * 
 * @property string $name
 * @property string $author
 * @property string $type
 * @property string $category
 * @property string $description
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    use HasFactory;
    protected $table = 'materials';
    protected $fillable = ['name','author', 'type', 'category','description'];

    
}

<?php
/**
 * @package App\Models
 * 
 * @property integer material_id Foreign key таблицы Material
 * @property integer tag_id Foreign key таблицы Tag
 * 
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tagmaterial extends Model
{
    use HasFactory;
    protected $table = 'tagsmaterial';
    protected $fillable = ['material_id', 'tag_id'];
}

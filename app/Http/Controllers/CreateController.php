<?php

namespace App\Http\Controllers;

use App\Models\Href;
use Illuminate\Http\Request;
use App\Models\Material;
use App\Models\Tagmaterial;
use App\Models\Category;
use App\Models\Tag;

class CreateController extends Controller
{
    public function createMaterial(Request $request){
        $data = new Data();
        $category = Category::all();
        $request->validate([
            'type' => 'required|numeric',
            'category' => 'required|numeric',
            'name' => 'required|string'
        ]);
            $material = new Material;
            $material->name = $request->name;
            $material->type = $data->type[$request->type];
            $material->category_id = $request->category;
            $material->author = $request->author;
            $material->description = $request->description;
            $material->save();

        return view('create-material')->with(['category' => $category,'type' => $data->type]);
    }

    public function updateMaterial(Request $request, int $id){
        $data = new Data();

        $request->validate([
            'type' => 'required|numeric',
            'category' => 'required|numeric',
            'name' => 'required|string'
        ]);
        if(is_numeric($request->type) && is_numeric($request->category)){
            Material::where('id', $id)->update([
                'name' => $request->name, 
                'author' => $request->author, 
                'type' => $data->type[$request->type],
                'category_id'=> $request->category,
                'description' => $request->description
            ]);
        }
        

        return redirect()->route('listMaterial');
    }
    public function deleteMaterial(Material $id){
        $id->delete();
        return redirect('/');
    }
    public function addTaginMaterial(int $id, Request $request){
        $tagmaterial = new Tagmaterial;
        $tagmaterial->material_id = $id;
        $tagmaterial->tag_id = $request->tag;
        $tagmaterial->save();

        return redirect()->route('viewMaterial', ['id' => $id]);
    }

    public function deleteTaginMaterial(Tagmaterial $idTag, $id){
        $idTag->delete();
        return redirect()->route('viewMaterial', ['id' => $id]);
    }
    public function addHref(Request $request, int $id){
        $request->validate([
            'name' => 'required',
            'href' => 'required'
        ]);

        $href = new Href;
        $href->material_id = $id;
        $href->name  = $request->name;
        $href->href = $request->href;
        $href->save();
        return redirect()->route('viewMaterial', ['id' => $id]);
    }
    public function deleteHrefinMaterial(Href $idHref, $id){
        $idHref->delete();
        return redirect()->route('viewMaterial', ['id' => $id]);
    }
    public function addCategory(Request $request){
        $request->validate([
            'name'=>'required'
        ]);
        $category = new Category;
        $category->category = $request->name;
        $category->save();
        return redirect()->route('listCategory');
    }
    public function addTag(Request $request){
        $request->validate([
            'tag'=>'required'
        ]);
        $tag = new Tag;
        $tag->tag = $request->tag;
        $tag->save();
        return redirect()->route('listTag');
    }
    public function deleteTag(Tag $id){
        $id->delete();
        return redirect()->route('listTag');
    }
    public function updateTag(Request $request, int $id){
        $request->validate([
            'tag' => 'required'
        ]);
        Tag::where('id',$id)->update([
            'tag' => $request->tag
        ]);
        return redirect()->route('listTag');
    }
    public function updateCategory(Request $request, int $id){
        $request->validate([
            'name' => 'required'
        ]);
        Category::where('id',$id)->update([
            'category' => $request->name
        ]);
        return redirect()->route('listCategory');
    }
    public function deleteCategory(Category $id){
        $id->delete();
        return redirect()->route('listCategory');
    }
}

class Data{
    public $type = [
        '0' => 'Книга',
        '1' => 'Статья',
        '2' => 'Видео',
        '3' => 'Сайт/Блог',
        '4' => 'Подборка',
        '5' => 'Ключевые идеи книги'
    ];
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Material;
use App\Models\Tag;
use App\Models\Tagmaterial;
use App\Models\Href;
use App\Models\Category;

class ViewController extends Controller
{
    public function viewMaterial(int $id){
        $material = Material::select('materials.id','name','type', 'description','category', 'author')->join('category', 'category_id', 'category.id')->findOrFail($id);
        $tags = Tag::all();
        $viewtags = Tagmaterial::select('tagsmaterial.id','tag')->where('material_id', $id)->join('tags', 'tag_id', 'tags.id')->get();
        $hrefs = Href::where('material_id',$id)->get();
        return view('view-material')->with(['material' => $material, 'tags' => $tags, 'id' => $id, 'viewtags' => $viewtags, 'hrefs' => $hrefs]);
    }

    public function viewListCategory(){
        $category = Category::all();
        return view('list-category')->with(['category'=>$category]);
    }

    public function viewListTag(){
        $tags = Tag::all();
        return view('list-tag')->with(['tags' => $tags]);
    }

    public function viewListMaterial(){
        $materials = Material::select('materials.id','category','name','author','type')->join('category', 'materials.category_id','category.id')->get();
        return view('list-materials')->with(['materials' => $materials]);
    }

    public function viewCreateCategory(){
        return view('create-category');
    }
    
    public function viewCreateMaterial(){
        $data = new Data;
        $category = Category::all();
        return view('create-material')->with(['category' => $category, 'type' => $data->type]);
    }
    public function viewCreateTag(){
        return view('create-tag');
    }
    public function viewUpdateMaterial(int $id){
        $data = new Data;
        $material = Material::findOrFail($id);
        $category = Category::all();
        return view('create-material')->with(['material'=>$material,'category' => $category,'type' => $data->type]);
    }
    public function search(Request $request){
        $materials = Tag::select('materials.id','category','name','author','tag','type')->join('tagsmaterial', 'tag_id','tags.id')->join('materials', 'material_id','materials.id')->join('category', 'materials.category_id', 'category.id')->where('name','like',$request->search . '%')->orWhere('author','like',$request->search . '%')->orWhere('tag','like',$request->search . '%')->orWhere('category','like',$request->search . '%')->get(); 
        return view('list-materials')->with(['materials' => $materials]);
    }
    public function viewUpdateTag(int $id){
        $tag = Tag::findOrFail($id);
        return view('create-tag')->with(['tag' =>$tag]);
    }
    public function viewUpdateCategory(int $id){
        $category = Category::findOrFail($id);
        return view('create-category')->with(['category' => $category]);
    }
    public function viewSearchTag(string $tag){
        $materials = Tag::select('materials.id','category','name','author','tag','type')->join('tagsmaterial', 'tag_id','tags.id')->join('materials', 'material_id','materials.id')->join('category', 'materials.category_id', 'category.id')->where('tag','like',$tag . '%')->get();
        return view('list-materials')->with(['materials' => $materials]);
    }
}

class Data{
    public $type = [
        '0' => 'Книга',
        '1' => 'Статья',
        '2' => 'Видео',
        '3' => 'Сайт/Блог',
        '4' => 'Подборка',
        '5' => 'Ключевые идеи книги'
    ];
}
